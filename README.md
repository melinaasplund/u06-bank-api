# project-u06-bank
A very simple Banking API.


## Getting started

```
python -m virtualenv env # Create a new python virtual environment.

source env/bin/activate # Enter into a python virtual environment.

pip install -r requirements.txt # Installs the packages defined in the file.

cd api

python app.py # Run the server

API endpoint can now be accessed via http://localhost:8000
or
http://localhost:8000/docs 

```

## Environment variables

```
export PRODUCTION=<bool> # set false to run it in development mode with live reload.
export DATABASE_URL=<username:password@host/db_name> # must be set
```

## Features

- User register endpoint

## Needs for the next sprint
- Show record of transaction
- User session
