from sqlmodel import Session
from database import engine
from models.user_model import Users, Credentials


def get_session():
    """
    Session generator opened in context manmager
    """
    with Session(engine) as session:
        yield session


def add_user(session, user, credentials):
    """
    This function adds new entries to tables Users and Credentials
    """
    try:
        new_user = Users(
            name=user.name, email=user.email, address=user.address
        )
        session.add(new_user)
        session.commit()

        new_user_credentials = Credentials(
            user_id=new_user.id,
            username=credentials.username, password=credentials.password
        )
        session.add(new_user_credentials)
        session.commit()
        return new_user, new_user_credentials

    except ValueError as err:
        return err
