from fastapi.testclient import TestClient
from app import app

client = TestClient(app.app)


def test_root_endpoint():
    response = client.get('/')
    assert response.status_code == 200
    assert response.json(), {"message": "Hello, world!"}


def test_register_endpoint():
    data = {
        "user": {
            "id": 0,
            "name": "John Doe",
            "email": "john@gmail.com",
            "address": "string"
        },
        "credentials": {
            "id": 0,
            "user_id": 0,
            "username": "string",
            "password": "string"
        }
    }
    response = client.post('/users/', json=data)

    assert response.status_code == 200
    # self.assertEqual(response.json(), 'null')
